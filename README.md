# psyngin
> version 1.0 | VisionMise

## Psyn Controller Daemon 
Passes requests to a designated input handler and waits for the result.

---

### How it works
psyngin runs a loop and waits for input from a *request* file. A unique ID is generated for the request and the contents of the file are replaced with the given ID.

The contents of the file are base64 encoded and passed to the configured handler. The standard output from the handler is read and written to a new file with the ID and a *.response* extension